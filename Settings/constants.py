# IDs for each player and chance
class Players(object):
    def __init__(self):
        self.CHANCE = 0
        self.P1 = 1
        self.P2 = 2


# IDs for terminal nodes (either after a fold or call action) and nodes that follow a check action
# @field terminal_fold (terminal node following fold) `-2`
# @field terminal_call (terminal node following call) `-1`
# @field chance_node (node for the chance player) `0`
# @field check (node following check) `-1`
# @field inner_node (any other node) `2`
class NodeTypes(object):
    def __init__(self):
        self.TERMINAL_FOLD = -2
        self.TERMINAL_CALL = -1
        self.CHECK = -1
        self.CHANCE_NODE = 0
        self.INNER_NODE = 1


# IDs for fold and check/call actions
# @field fold `-2`
# @field ccall (check/call) `-1`
class Actions(object):
    def __init__(self):
        self.FOLD = -2
        self.CCALL = -1


# String representations for actions in the ACPC protocol
# @field fold "`fold`"
# @field ccall (check/call) "`ccall`"
# @field raise "`raise`"
class ACPCActions(object):
    def __init__(self):
        self.FOLD = "fold"
        self.CCALL = "ccall"
        self.RAISE = "raise"


class Constants(object):
    def __init__(self):
        # the number of players in the game
        self.PLAYERS_COUNT = 2
        # the number of betting rounds in the game
        self.STREETS_COUNT = 2

        self.players = Players()
        self.actions = Actions()
        self.acpc_actions = ACPCActions()


constants = Constants()
