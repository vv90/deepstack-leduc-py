import torch


class Arguments(object):
    def __init__(self):
        self.BET_SIZING = {1}
        self.STREETS_COUNT = 2
        self.TENSOR = torch.FloatTensor
        self.ANTE = 100
        self.STACK = 1200

        # the number of iterations that DeepStack runs CFR for
        self.CFR_ITERS = 1000

        # the number of preliminary CFR iterations which DeepStack doesn't factor into the average strategy (included in CFR_ITERS)
        self.CFR_SKIP_ITERS = 500

        self.LEARNING_RATE = 0.001


arguments = Arguments()
