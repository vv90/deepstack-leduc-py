
class GameSettings(object):
    def __init__(self):
        self.SUIT_COUNT = 2
        self.RANK_COUNT = 3
        self.CARD_COUNT = self.SUIT_COUNT * self.RANK_COUNT
        self.BOARD_CARD_COUNT = 1
        self.PLAYER_COUNT = 2


game_settings = GameSettings()
